package server;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import server.TicTacToeServer.ClientServiceThread;
import server.game.TableXO;

class Table {
    boolean disconnect;

	private Socket player1 = new Socket();
	private Socket player2 = new Socket();

	private TableXO table;

    private boolean p1Rematch;
    private boolean p2Rematch;

	private boolean sequenceOfMoves;
    private boolean swapId;

    String p1Name;
    String p2Name;

	private PrintWriter out1;
	private PrintWriter out2;

	Table() {
		table = new TableXO();

		Thread checkOnline = new Thread(new CheckOnline());
		checkOnline.start();
	}

	short addPlayer(ClientServiceThread player){
	    try {
            if (!player1.isBound()) {
                player1 = player.getSocket();
                p1Name = player.getName();
                out1 = new PrintWriter(new OutputStreamWriter(player1.getOutputStream()));

                return 1;
            }

            else if (!player2.isBound()) {
                player2 = player.getSocket();
                p2Name = player.getName();
                out2 = new PrintWriter(new OutputStreamWriter(player2.getOutputStream()));


                out1.print("GAME_START!");
                out2.print("GAME_START!");

                out1.flush();
                out2.flush();

                return 2;
            }
        } catch (IOException ioe){
	        ioe.printStackTrace();
        }

        return 0;
	}

	synchronized void turn(String message, short id){
        if(!swapId) {

            if (!sequenceOfMoves && !table.gameOver && id == 1) {
                if (!xTurn(message))
                    return;

                sendTable();

                if (table.gameOver)
                    p1WinNotification();

                sequenceOfMoves = true;
            }

                else if (sequenceOfMoves && !table.gameOver && id == 2) {
                if (!oTurn(message))
                    return;

                sendTable();

                if (table.gameOver)
                    p2WinNotification();

                sequenceOfMoves = false;
            }
        }

        else {
            if (!sequenceOfMoves && !table.gameOver && id == 2) {
                if(!xTurn(message))
                    return;

                sendTable();

                if (table.gameOver)
                    p2WinNotification();

                sequenceOfMoves = true;
            }

            else if (sequenceOfMoves && !table.gameOver && id == 1) {
                if(!oTurn(message))
                    return;

                sendTable();

                if (table.gameOver)
                    p1WinNotification();

                sequenceOfMoves = false;
            }
        }

        if(table.checkDraw()){
            out1.print("@DRAW");
            out1.flush();

            out2.print("@DRAW");
            out2.flush();
        }
    }

    private void p1WinNotification(){
        out1.print("@WIN!");
        out1.flush();

        out2.print("@LOSE!");
        out2.flush();
    }

    private void p2WinNotification(){
        out2.print("@WIN!");
        out2.flush();

        out1.print("@LOSE!");
        out1.flush();
    }

	private void sendTable() {
		out1.print("# " + table + "!");
		out2.print("# " + table + "!");

		out1.flush();
		out2.flush();
	}

	private boolean xTurn(String message){
        String[] coordinates = message.split(" ");

        int x = Integer.parseInt(coordinates[0]);
        int y = Integer.parseInt(coordinates[1]);

        return table.setX(x, y);
    }

	private boolean oTurn(String message){
        String[] coordinates = message.split(" ");

        int x = Integer.parseInt(coordinates[0]);
        int y = Integer.parseInt(coordinates[1]);

        return table.setO(x, y);
    }

	private boolean isFree() {
		return !(player1.isBound() && player2.isBound());
	}

	String showTable() {
		String s;

		if (!this.isFree()) {
			s = p1Name + " " + p2Name;
		} else if (player1.isBound()) {
			s = p1Name;
		} else {
			s = p2Name;
		}

		return s;
	}

	synchronized void quitBoard(int id){
	    if(id == 1) {
            out2.print("@ " + p1Name + " EXIT!");
            out2.flush();

            disconnect = true;
        }
	    if(id == 2) {
            out1.print("@ " + p2Name + " EXIT!");
            out1.flush();

            disconnect = true;
        }
    }

    void rematch(int id){
	    if(id == 1)
	        p1Rematch = true;

        if(id == 2)
	        p2Rematch = true;

        if(p1Rematch && p2Rematch){
            table.rematch();
            sendTable();

            sequenceOfMoves = false;

            p1Rematch = false;
            p2Rematch = false;

            swapId = !swapId;

            out1.print("@REMATCH " + swapId + "!");
            out2.print("@REMATCH " + swapId + "!");

            out1.flush();
            out2.flush();
        }
    }

	public class CheckOnline extends Thread{
	    public void run() {
            while(!disconnect) synchronized (this) {

                if (!isFree()) {
                    if (player1.isClosed()) {
                        out2.print("@ " + p1Name + " EXIT!");
                        out2.flush();

                        disconnect = true;
                    }

                    if (player2.isClosed()) {
                        out1.print("@ " + p2Name + " EXIT!");
                        out1.flush();

                        disconnect = true;
                    }
                }
                else{
                    if(player1.isClosed()){
                        disconnect = true;
                    }
                }
            }
        }
    }
}