package server.game;

class WinCheck {
	private int [] array1;   //  |
	private int [][] array2; //  \
	private int [][] array3; //  /
	
	private int [][] tbl;
	
	WinCheck(int [][] t){
		tbl = new int[t.length][t.length];
		tbl = t;
		
		array1 = new int[5];
		array2 = new int[5][5];
		array3 = new int[5][5];
		
		for(int i = 0; i < array1.length; i++)
				array1[i] = 3;
				
		for(int i = 0; i < array2.length; i++)
			for(int j = 0; j < array2.length; j++){
				if(i == j)
					array2[i][j] = 3;
				else
					array2[i][j] = 0;
				
				if( (i + j) + 1 == array2.length)
					array3[i][j] = 3;
				else
					array3[i][j] = 0;
			}
	}
	void convertTo(int n){ // 1 - X , 2 - O
		for(int i = 0; i < array1.length; i++)
			array1[i] = n;
		
		for(int i = 0; i < array2.length; i++){
			for(int j = 0; j < array2.length; j++){
				if(array2[i][j] == 3)
					array2[i][j] = n;
				
				if(array3[i][j] == 3)
					array3[i][j] = n;
			}
		}
	}
	
	boolean check(){
		int count1 = 0;
		int count2 = 0;
		
		for(int m = 0; m < tbl.length; m++){
			for(int n = 0; n < tbl.length; n++){
				for(int i = m, k = 0; i < m + array2.length; i++, k++){
					if( i >= tbl.length)
						break;
					
					for(int j = n , l = 0; j < n + array2.length; j++ , l++){
						if( j >= tbl.length)
							break;
						
						if(array2[k][l] != 0)
							if(array2[k][l] == tbl[i][j])
								count1++;

						if(array3[k][l] != 0)
							if(array3[k][l] == tbl[i][j])
								count2++;
					}
				}
				if(count1 == 5 || count2 == 5)
					return true;
				count1 = count2 = 0;
			}
		}
		
		for(int m = 0; m < tbl.length; m++){
			for(int n = 0; n < tbl.length; n++){
				for(int j = n, k = 0; j < n + array2.length; j++, k++){
					if( j >= tbl.length)
						break;
					
					if(array1[k] == tbl[m][j])
							count1++;
					if(array1[k] == tbl[j][m])
							count2++;
				}
				if(count1 == 5 || count2 == 5)
					return true;
				count1 = count2 = 0;
			}
		}
		return false;
	}
}
