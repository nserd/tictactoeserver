package server.game;

public class TableXO {
	private int [][] table;
	
	public boolean gameOver;
	
	public TableXO(){
		table = new int[10][10];
		
		for(int i = 0; i < table.length; i++){
			for(int j = 0; j < table.length; j++){
				table[i][j] = 0;
			}
		}
	}
	
	public boolean setX(int i, int j){
		if( table [i][j] != 0)
			return false;
		else{
			table[i][j] = 1;
			WinCheck wC = new WinCheck(table);
			wC.convertTo(1);
			
			if(wC.check())
				gameOver = true;

			return true;
		}
	}
	
	public boolean setO(int i, int j){
		if( table [i][j] != 0)
			return false;
		else{
			table[i][j] = 2;

			WinCheck wC = new WinCheck(table);
			wC.convertTo(2);
			
			if(wC.check())
				gameOver = true;
			
			return true;
		}
	}

	public void rematch(){
        for(int i = 0; i < table.length; i++){
            for(int j = 0; j < table.length; j++){
                table[i][j] = 0;
            }
        }

        gameOver = false;
    }

    public boolean checkDraw(){
        for(int i = 0; i < table.length; i++){
            for(int j = 0; j < table.length; j++){
                if(table[i][j] == 0){
                    return false;
                }
            }
        }

        return true;
    }

	public String toString(){
		StringBuilder s = new StringBuilder("");

		for(int i = 0; i < table.length; i++){
			for(int j = 0; j < table.length; j++){
				s.append(table[i][j]).append(" ");
			}
			s.append('\n');
		}
		
		return s.toString();
	}
}
