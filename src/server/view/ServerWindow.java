package server.view;

import server.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ServerWindow extends JFrame{

    private JTextArea clientText;

    public ServerWindow(){
        super("Tic-Tac-Toe server");
        setMinimumSize(new Dimension(330,285));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        clientText = new JTextArea();
        JButton clearLog = new JButton("Clear log file");

        JScrollPane scrollPaneClient = new JScrollPane(clientText);
        scrollPaneClient.setPreferredSize(new Dimension(300,200));

        clientText.setWrapStyleWord(true);
        clientText.setEditable(false);

        clearLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Log(true);
            }
        });

        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        c.add(scrollPaneClient);
        c.add(clearLog, BorderLayout.SOUTH);
        setVisible(true);
    }

    public void print(String data){
        clientText.append(data + "\n");
        clientText.setCaretPosition(clientText.getText().length());
        repaint();
    }
}
