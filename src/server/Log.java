package server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

public class Log {
    private FileWriter logWriter;

    public Log(boolean clear){
        try {
            File log = new File(currentPath() + "/log.txt");

            //noinspection ResultOfMethodCallIgnored
            log.createNewFile();

            if(!clear)
                logWriter = new FileWriter(log, true);

            else{
                logWriter = new FileWriter(log, false);
                logWriter.write("");
                logWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String currentPath(){
        URL url = getClass().getResource("/server");
        String decodedURL;

        StringBuilder path = new StringBuilder("/");

        try {
            decodedURL = URLDecoder.decode(url.toString(), "UTF-8");

            String[] buff = decodedURL.split("/");

            for(int i = 1; i < buff.length - 2; i++){
                path.append(buff[i]);
                path.append("/");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return path.toString();
    }

    void write(String string){
        try {
            logWriter.write(string + '\n');
            logWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
