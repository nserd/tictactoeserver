package server;

import server.view.ServerWindow;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TicTacToeServer extends Thread{
    private static Log log;
    private static ServerWindow window;

    private static ServerSocket myServerSocket;

    private static ArrayList<ClientServiceThread> allClients = new ArrayList<>();
    private static Map<Integer, Table> tables = new ConcurrentHashMap<>();

    private static int clientIndex;
    private static int tableIndex;

    private static final Object o = new Object();

    public TicTacToeServer() {
        int port = 11111;

        try {
            myServerSocket = new ServerSocket(port);
            clientIndex = 0;
        } catch (IOException ioe) {
            window.print("Could not create server socket on port " + port);
            window.print("Quitting...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.exit(-1);
        }

        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

        window.print("Server started.");
        window.print("It is now : " + formatter.format(now.getTime()));

        log.write("----------" + formatter.format(now.getTime()) + "----------");
    }

    public void run(){
        //noinspection InfiniteLoopStatement
        while (true) try {
            Socket clientSocket = myServerSocket.accept();

            ClientServiceThread cliThread = new ClientServiceThread(clientSocket);
            allClients.add(clientIndex, cliThread);

            cliThread.start();
            clientIndex++;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args) {
        window = new ServerWindow();
        log = new Log(false);

        Thread ms = new Thread(new TicTacToeServer());
        ms.start();

        //noinspection InfiniteLoopStatement
        while(true) synchronized (o){
            if(!tables.isEmpty()) {
                Set<Map.Entry<Integer, Table>> set = tables.entrySet();

                for (Map.Entry<Integer, Table> table : set) {
                    if(table.getValue().disconnect){
                        log.write(">Table-" + table.getKey() + " \"" +
                                  table.getValue().showTable() + "\"" + " deleted.");

                        set.remove(table);
                    }
                }
            }

            if(!allClients.isEmpty()){
                for(int i = 0; i < allClients.size(); i++){
                    if(allClients.get(i).getSocket().isClosed()){
                        allClients.remove(i);
                        clientIndex--;
                    }
                }
            }
        }
    }

    class ClientServiceThread extends Thread {
        private Socket clientSocket;

        private boolean stopClientThread;

        private int currentTableIndex;
        short numbInTable;

        ClientServiceThread(Socket s) {
            clientSocket = s;
        }

        public void run() {
            BufferedReader in;
            PrintWriter out;

            window.print("Accepted Client Address - " + clientSocket.getInetAddress().getHostName());
            log.write(">Accepted Client Address - " + clientSocket.getInetAddress().getHostName());

            try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                while (!stopClientThread){
                    String message = in.readLine();

                    if (!message.equals("")) {
                        commands(message, out);

                        log.write("Client " + getName() + ": " + message.trim());
                    }
                }

            } catch (SocketException | NullPointerException se) {
                window.print(getName() + " disconnected.");
                log.write(">" + getName() + " disconnected.");

                try {
                    clientSocket.close();
                    stopClientThread = true;
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        private void commands(String message, PrintWriter out) {
            /* PLAY
             * # coordinateX coordinateY
             * GET_TABLES
             * STOP
             * CREATE_NEW_TABLE
             * ENTER_TABLE tableNumber
             * GET_NAMES
             * REMATCH
             * DELETE
             * NAME
            */

            if (message.equals("PLAY")) {
                if (tables.isEmpty()) {
                    createTableAndEnter(out);
                } else {
                    boolean done = false;

                    Set<Map.Entry<Integer, Table>> set = tables.entrySet();

                    for (Map.Entry<Integer, Table> table : set) {
                        numbInTable = table.getValue().addPlayer(this);

                        if (numbInTable != 0) {
                            done = true;
                            currentTableIndex = table.getKey();

                            break;
                        }
                    }

                    if(!done)
                        createTableAndEnter(out);
                }
            }

            else if(message.charAt(0) == '#'){
                String[] buff = message.split(" ", 2);
                tables.get(currentTableIndex).turn(buff[1], numbInTable);
            }

            else if (message.equals("GET_TABLES")) {
                if (tables.isEmpty()) {
                    out.print("$NO_TABLES!");
                    out.flush();
                }

                else {
                    StringBuilder s = new StringBuilder("");

                    s.append("$ ");

                    Set<Map.Entry<Integer, Table>> set = tables.entrySet();

                    for (Map.Entry<Integer, Table> table : set)
                        s.append(table.getValue().showTable()).append('\n');

                    s.append('!');

                    out.print(s.toString());
                    out.flush();
                }
            }

            else if (message.equals("STOP")) {
                tables.get(currentTableIndex).quitBoard(numbInTable);
            }

            else if (message.equals("CREATE_NEW_TABLE")) {
                createTableAndEnter(out);
                out.flush();
            }

            else if (message.contains("ENTER_TABLE")) {
                String[] buff = message.split(" ");

                boolean done = false;

                Set<Map.Entry<Integer, Table>> set = tables.entrySet();

                for (Map.Entry<Integer, Table> table : set) {
                    if(table.getValue().showTable().equals(buff[1])){
                        done = true;

                        numbInTable = tables.get(table.getKey()).addPlayer(this);

                        if (numbInTable != 0) {
                            currentTableIndex = table.getKey();

                            break;
                        }

                        else {
                            out.print("$ERROR!");
                            out.flush();

                            break;
                        }
                    }
                }

                if(!done) {
                    out.print("$ERROR!");
                    out.flush();
                }
            }

            else if(message.equals("GET_NAMES")){
                out.print("@NAMES " +
                        tables.get(currentTableIndex).p1Name + " " +
                        tables.get(currentTableIndex).p2Name + " " +
                        numbInTable + "!");

                out.flush();
            }

            else if(message.equals("REMATCH")){
                tables.get(currentTableIndex).rematch(numbInTable);
            }

            else if(message.equals("DELETE")){
                tables.get(currentTableIndex).disconnect = true;
            }

            else if (message.contains("NAME")) {
                String[] buff = message.split(" ", 2);
                setName(buff[1]);

                window.print(getName() + " connected.");
                log.write(">" + getName() + " connected.");
            }
        }

        private void createTableAndEnter(PrintWriter out){
            tables.put(tableIndex, new Table());
            numbInTable = tables.get(tableIndex).addPlayer(this);

            log.write(">Table-" + tableIndex + " \"" + tables.get(tableIndex).showTable() + "\"" + " created.");

            currentTableIndex = tableIndex;
            tableIndex++;

            out.print("WAIT!");
            out.flush();
        }

        Socket getSocket() {
            return clientSocket;
        }
    }
}