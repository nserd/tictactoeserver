import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import server.game.TableXO;

import java.util.Random;

public class TableTest {
    private TableXO table = new TableXO();

    @After
    public void cleanTable(){
        table = new TableXO();
    }

    @Test
    public void setX_Test(){
        Random rand = new Random();

        int i = rand.nextInt(9);
        int j = rand.nextInt(9);

        table.setX(i,j);


        String[] buffRows = table.toString().split("\n");
        String[] buffColumns = buffRows[i].split(" ");

        Assert.assertTrue(Integer.parseInt(buffColumns[j]) == 1);
    }

    @Test
    public void setO_Test(){
        Random rand = new Random();

        int i = rand.nextInt(9);
        int j = rand.nextInt(9);

        table.setO(i,j);


        String[] buffRows = table.toString().split("\n");
        String[] buffColumns = buffRows[i].split(" ");

        Assert.assertTrue(Integer.parseInt(buffColumns[j]) == 2);
    }

    @Test
    public void setInFullCell_Test(){
        boolean flag1;
        boolean flag2;

        table.setO(5,5);
        flag1 = table.setX(5,5);

        table.setX(3,3);
        flag2 = table.setO(3,3);

        Assert.assertFalse(flag1 && flag2);
    }

    @Test
    public void rematch_Test(){
        Random rand = new Random();

        int count = 0;

        boolean flag = true;

        while(count < 5) {
            table.setX(rand.nextInt(9), rand.nextInt(9));
            table.setO(rand.nextInt(9), rand.nextInt(9));

            count++;
        }

        table.rematch();

        String[] buffRows = table.toString().split("\n");
        for(int i = 0; i < buffRows.length; i++) {
            String[] buffColumns = buffRows[i].split(" ");

            for(int j = 0; j < buffColumns.length; j++){
                if(Integer.parseInt(buffColumns[j]) != 0){
                    flag = false;
                    break;
                }
            }
        }

        Assert.assertTrue(flag);
    }

    @Test
    public void checkDraw_Test(){
        boolean draw;
        boolean notDraw;

        notDraw = table.checkDraw();

        for (int i = 0; i < 10; i++){
            for (int j = 0; j < 10; j++){

                if ((i / 2) % 2 == 0)
                    if (j % 2 == 0)
                        table.setX(i, j);
                    else
                        table.setO(i, j);

                else
                    if (j % 2 != 0)
                        table.setX(i, j);
                    else
                        table.setO(i, j);
            }
        }

        draw = table.checkDraw();

        Assert.assertTrue((notDraw ^ draw) && draw);
    }
}
